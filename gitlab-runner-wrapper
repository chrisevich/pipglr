#!/bin/bash

# This script is intended to be called as the entrypoint for
# a podman-in-podman gitlab runner container.  Any usage
# outside that context is not supported and may cause harm.

set -e

unset _debug_args
if [[ -n "$PODMAN_RUNNER_DEBUG" ]]; then
    _debug_args="--log-level=$PODMAN_RUNNER_DEBUG"
fi

SOCKET="/tmp/podman-run-1000/podman/podman.sock"
if [[ "$1" == "run" ]] && [[ ! -S "$SOCKET" ]]; then
    podman $_debug_args system service -t 0 &
    /usr/local/bin/podman-in-podman-maintenance &
    # Prevent SIGHUP propagation to podman process
    disown -ar
    sleep 1s  # Give podman a chance to get going
    # Verify podman is listening on it's socket
    if [[ ! -S "$SOCKET" ]]; then
        echo "ERROR: Inner-podman system service failed to start, expecting to find socket '$SOCKET'. Are all volume's owned & writeable by $(id -u podman):$(id -g podman)?" > /dev/stderr
        exit 1
    fi
fi

exec gitlab-runner "$@"
