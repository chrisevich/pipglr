#!/bin/bash

# This script is intended to be called by the entrypoint for
# a podman-in-podman gitlab runner container.  Any usage
# outside that context is not supported and may cause harm.

set -eo pipefail

maintain_podman() {
  # Two days seems to be a good happy-medium beween filling up
  # about 40gig of storage space from moderate CI activity,
  # and maintaining a useful level of caching.
  while sleep "$CLEAN_INTERVAL"; do
    if [[ -n "$PODMAN_RUNNER_DEBUG" ]]; then
      echo "$(date --iso-8601=second) ${BASH_SOURCE[0] performing podman maintenance}"
    fi
    podman system prune --all --forcea || true
    podman rmi -af || true
  done
}

if [[ -z "$CLEAN_INTERVAL" ]]; then
  echo "ERROR: Empty/unset \$CLEAN_INTERVAL"
  exit 1
fi

maintain_podman
